//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

struct PSInput
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
	float2 texCoord : TEXCOORD0;
};

PSInput VSMain(float4 position : POSITION, float4 color : COLOR, float2 texCoord : TEXCOORD0)
{
    PSInput result;
    result.position = position;
    result.color = color;
	result.texCoord = float2(position.x, position.y);
	return result;
}

float4 PSMain(PSInput input) : SV_TARGET
{
	float dist = input.texCoord[0] * input.texCoord[0]
               + input.texCoord[1] * input.texCoord[1];
	if (dist < 0.25)
		return float4(1, 1, 1, 1);
	else
		return float4(0, 0, 0, 1);
}
