# Vkd3d-shader Enhancements - Documentation Improvements
Author: Qingyi Wang
## 1. Initialization
### 1.1 Window Creation
In Windows, we can use *CreateWindow()* or *CreateWindowEx()* to create a window, and use *ShowWindow()* to display it.  

In Linux, we can mainly use *xcb_create_window()* to create a window and use *xcb_map_window()* to show it instead.
```c
//Create a window
xcb_void_cookie_t xcb_create_window (
    xcb_connection_t                          *c,//The connection
    uint8_t                                   depth, //The new window's depth 
    xcb_window_t                              wid, //The ID with which you will refer to the new window
    xcb_window_t                              parent, //The parent window of the new window.
    int16_t                                   x, //The X coordinate of the new window.
    int16_t                                   y, //The Y coordinate of the new window.
    uint16_t                                  width, //The width of the new window.
    uint16_t                                  height, //The height of the new window.
    uint16_t                                  border_width,
    uint16_t                                  _class,
    xcb_visualid_t                            visual,
    uint32_t                                  value_mask, //A bitmask of #xcb_cw_t values.
    const void                                *value_list);

//Map the specified window. This means making the window visible.
xcb_void_cookie_t xcb_map_window (
    xcb_connection_t                          *c,
    xcb_window_t                              window);
```
In fact, this step would be a little more complicated in Linux. An easy example will be given as follows. 
```c
//1. Define and initialize connection, window and screen
//Set 0 as the screen number
int screen_num = 0;
//Connect to the X server using xcb_connect()
//The name of the display is NULL
xcb_connection_t *connection = xcb_connect(NULL, &screen_num);
//Allocate an XID for this connection
xcb_window_t window = xcb_generate_id(connection);
//Get xcb_screen_t data
xcb_screen_iterator_t iter = xcb_setup_roots_iterator(xcb_get_setup(connection));
xcb_screen_t *screen = iter.data;
//Set the width and height of the window
unsigned int width = 640;
unsigned int height = 480;
//Set window events TODO
static const uint32_t window_events = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS;

//2. Create a window using xcb_create_window()
//The special value `XCB_COPY_FROM_PARENT` means the depth is taken from the parent window.
xcb_create_window(connection, XCB_COPY_FROM_PARENT, window, screen->root, 0, 0,
            width, height, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual,
            XCB_CW_EVENT_MASK, &window_events);

//3. Make this window visible using xcb_map_window()
xcb_map_window(connection, window);

//4. Force any buffered output to be written to the server using xcb_flush()
xcb_flush(connection);
```
After creating a window, you need to store the value of *connection, screen & window*. They will be used in the following steps. So it would be better for you to define the structure of your demo and demo window and just use them in the functions. If you want to change the property of the window, you can use the function *xcb_change_property()*. I would provide more details for you which could be seen in *demos/demo_xcb.h*. 
```c
//demo_xcb.h

//Declare demo struct type
struct demo
{
    xcb_connection_t *connection;
    xcb_atom_t wm_protocols_atom;
    xcb_atom_t wm_delete_window_atom;
    xcb_key_symbols_t *xcb_keysyms;
    int screen;

    struct demo_window **windows;
    size_t windows_size;
    size_t window_count;

    void *user_data;
    void (*idle_func)(struct demo *demo, void *user_data);
};

//Declare demo_window struct type
struct demo_window
{
    xcb_window_t window;
    struct demo *demo;

    void *user_data;
    void (*expose_func)(struct demo_window *window, void *user_data);
    void (*key_press_func)(struct demo_window *window, demo_key key, void *user_data);
};

//Declare cxt_fence struct type
struct cxt_fence
{
    ID3D12Fence *fence;
    UINT64 value;
    HANDLE event;
};

//Get the specific index of xcb_screen_t data from xcb_connection_t object
static inline xcb_screen_t *demo_get_screen(xcb_connection_t *c, int idx)
{
    //Define the xcb_screen_iterator_t object
    xcb_screen_iterator_t iter;

    ///xcb_get_setup(): Accessor for the xcb_setup_t data returned by the server when the xcb_connection_t was initialized
    ///xcb_setup_roots_iterator(): Set the root iterator from the xcb_setup_t data

    //Get the root iterator of xcb_connection_t data
    iter = xcb_setup_roots_iterator(xcb_get_setup(c));
    //Find the corresponding xcb_screen_t data by using the iterator
    for (; iter.rem; xcb_screen_next(&iter), --idx)
    {
        if (!idx)
            return iter.data;
    }

    return NULL;
}

//Add demo_window data to demo
static inline bool demo_add_window(struct demo *demo, struct demo_window *window)
{
    if (demo->window_count == demo->windows_size)
    {
        size_t new_capacity;
        void *new_elements;

        new_capacity = max(demo->windows_size * 2, 4);
        if (!(new_elements = realloc(demo->windows, new_capacity * sizeof(*demo->windows))))
            return false;
        demo->windows = new_elements;
        demo->windows_size = new_capacity;
    }

    demo->windows[demo->window_count++] = window;

    return true;
}

//Create the window for the demo
static inline struct demo_window *demo_window_create(struct demo *demo, const char *title,
        unsigned int width, unsigned int height, void *user_data)
{
    static const uint32_t window_events = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS;

    struct demo_window *window;
    xcb_size_hints_t hints;
    xcb_screen_t *screen;

    if (!(screen = demo_get_screen(demo->connection, demo->screen)))
        return NULL;

    if (!(window = malloc(sizeof(*window))))
        return NULL;

    if (!demo_add_window(demo, window))
    {
        free(window);
        return NULL;
    }

    window->window = xcb_generate_id(demo->connection);
    window->demo = demo;
    window->user_data = user_data;
    window->expose_func = NULL;
    window->key_press_func = NULL;
    //Create a window
    xcb_create_window(demo->connection, XCB_COPY_FROM_PARENT, window->window, screen->root, 0, 0,
            width, height, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual,
            XCB_CW_EVENT_MASK, &window_events);
    //change the property of the window
    xcb_change_property(demo->connection, XCB_PROP_MODE_REPLACE, window->window, XCB_ATOM_WM_NAME,
            XCB_ATOM_STRING, 8, strlen(title), title);
    xcb_change_property(demo->connection, XCB_PROP_MODE_REPLACE, window->window, demo->wm_protocols_atom,
            XCB_ATOM_ATOM, 32, 1, &demo->wm_delete_window_atom);
    hints.flags = XCB_ICCCM_SIZE_HINT_P_MIN_SIZE | XCB_ICCCM_SIZE_HINT_P_MAX_SIZE;
    hints.min_width = width;
    hints.min_height = height;
    hints.max_width = width;
    hints.max_height = height;
    xcb_change_property(demo->connection, XCB_PROP_MODE_REPLACE, window->window, XCB_ATOM_WM_NORMAL_HINTS,
            XCB_ATOM_WM_SIZE_HINTS, 32, sizeof(hints) >> 2, &hints);
    //Make this window visible
    xcb_map_window(demo->connection, window->window);
    //Force any buffered output to be written to the server
    xcb_flush(demo->connection);

    return window;
}

//main.c

//Declare cx_test struct type
struct cx_test
{
    struct demo demo;

    struct demo_window *window;

    unsigned int width;
    unsigned int height;
    float aspect_ratio;

    D3D12_VIEWPORT vp;
    D3D12_RECT scissor_rect;

    ID3D12Device *device;
    ID3D12CommandQueue *command_queue;
    struct demo_swapchain *swapchain;
    ID3D12DescriptorHeap *rtv_heap;
    unsigned int rtv_descriptor_size;
    ID3D12Resource *render_targets[3];
    ID3D12CommandAllocator *command_allocator;

    ID3D12RootSignature *root_signature;
    ID3D12PipelineState *pipeline_state;
    ID3D12GraphicsCommandList *command_list;
    ID3D12Resource *vb;
    D3D12_VERTEX_BUFFER_VIEW vbv;

    unsigned int frame_idx;
    struct cxt_fence fence;
};

struct cx_test cxt;
unsigned int width = 640, height = 480;
cxt.window = demo_window_create(&cxt.demo, "Vkd3d Test", width, height, &cxt);
```
### 1.2 Device Creation
In Windows, we can use *ID3D12Device::D3D12CreateDevice()* to create device.  
```c
HRESULT D3D12CreateDevice(
    IUnknown                                  *pAdapter,
    D3D_FEATURE_LEVEL                         MinimumFeatureLevel, 
    REFIID                                    riid,
    void                                      **ppDevice);
```
In vkd3d, we can also use *D3D12CreateDevice()* which includes the *same* 4 parameters.
```c
#define D3D12CreateDevice(a, b, c, d) D3D12CreateDeviceVKD3D(a, b, c, d, VKD3D_UTILS_API_VERSION)
HRESULT WINAPI D3D12CreateDeviceVKD3D(
    IUnknown                                  *adapter, 
    D3D_FEATURE_LEVEL                         feature_level,
    REFIID                                    iid, 
    void                                      **device, 
    enum                                      vkd3d_api_version api_version);
```
We can use this function in Linux easily just like what we do in Windows.
For exmaple,
```c
HRESULT hr = D3D12CreateDevice(NULL, D3D_FEATURE_LEVEL_11_0, &IID_ID3D12Device, (void **)&device);
```
## 2. Loading Pipeline
### 2.1 Create Command Queue
We can use *ID3D12Device_CreateCommandQueue()* to create the command queue in Linux.
```c
HRESULT ID3D12Device_CreateCommandQueue(
    ID3D12Device                              *This,
    const D3D12_COMMAND_QUEUE_DESC            *desc,
    REFIID                                    riid,
    void                                      **command_queue);
```
This step in Linux is very similar to the step in Windows. Let me show you in the following example.
```c
D3D12_COMMAND_QUEUE_DESC queue_desc;
HRESULT hr = ID3D12Device_CreateCommandQueue(cxt->device, &queue_desc,
            &IID_ID3D12CommandQueue, (void **)&cxt->command_queue);
```
### 2.2 Create SwapChain
In Windows, we can use *IDXGIFactory2::CreateSwapChainForHwnd()* to create the swapchain.
```c
HRESULT CreateSwapChainForHwnd(
    IUnknown                                  *pDevice,  
    HWND                                      hWnd,
    const DXGI_SWAP_CHAIN_DESC1               *pDesc,
    const DXGI_SWAP_CHAIN_FULLSCREEN_DESC     *pFullscreenDesc,  
    IDXGIOutput                               *pRestrictToOutput,    
    IDXGISwapChain1                           **ppSwapChain);
``` 
In Linux, struct DXGI_SWAP_CHAIN_DESC1 is not existing.Instead, we would use some functions in vulkan such as  *vkCreateSwapchainKHR()* to create the swapchain which I would list as follows.
```c
//get vk format
VkFormat vkd3d_get_vk_format(DXGI_FORMAT format)
//get vkd3d instance
vkd3d_instance *vkd3d_instance_from_device(ID3D12Device *device)
//get vk_instance from vkd3d instance
VkInstance vkd3d_instance_get_vk_instance(struct vkd3d_instance *instance)
//get vk physical device
VkPhysicalDevice vkd3d_get_vk_physical_device(ID3D12Device *device);
//get vk device
VkDevice vkd3d_get_vk_device(ID3D12Device *device);
//create vk surface
VkResult vkCreateXcbSurfaceKHR(
    VkInstance                                instance,
    const VkXcbSurfaceCreateInfoKHR*          pCreateInfo,
    const VkAllocationCallbacks*              pAllocator,
    VkSurfaceKHR*                             pSurface);

//vulkan_core.h
VKAPI_ATTR VkResult VKAPI_CALL vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
    VkPhysicalDevice                            physicalDevice,
    VkSurfaceKHR                                surface,
    VkSurfaceCapabilitiesKHR*                   pSurfaceCapabilities);
VKAPI_ATTR VkResult VKAPI_CALL vkGetPhysicalDeviceSurfaceFormatsKHR(
    VkPhysicalDevice                            physicalDevice,
    VkSurfaceKHR                                surface,
    uint32_t*                                   pSurfaceFormatCount,
    VkSurfaceFormatKHR*                         pSurfaceFormats);
//create swapchain**
VkResult vkCreateSwapchainKHR(
    VkDevice                                  device,
    const VkSwapchainCreateInfoKHR*           pCreateInfo,
    const VkAllocationCallbacks*              pAllocator,
    VkSwapchainKHR*                           pSwapchain);
//create surface
VKAPI_ATTR VkResult VKAPI_CALL vkCreateFence(
    VkDevice                                  device,
    const VkFenceCreateInfo*                  pCreateInfo,
    const VkAllocationCallbacks*              pAllocator,
    VkFence*                                  pFence);
VkResult VKAPI_CALL vkGetSwapchainImagesKHR(
    VkDevice                                  device,
    VkSwapchainKHR                            swapchain,
    uint32_t*                                 pSwapchainImageCount,
    VkImage*                                  pSwapchainImages);
VkResult VKAPI_CALL vkAcquireNextImageKHR(
    VkDevice                                  device,
    VkSwapchainKHR                            swapchain,
    uint64_t                                  timeout,
    VkSemaphore                               semaphore,
    VkFence                                   fence,
    uint32_t*                                 pImageIndex);
VKAPI_ATTR VkResult VKAPI_CALL vkWaitForFences(
    VkDevice                                    device,
    uint32_t                                    fenceCount,
    const VkFence*                              pFences,
    VkBool32                                    waitAll,
    uint64_t                                    timeout);
VKAPI_ATTR VkResult VKAPI_CALL vkResetFences(
    VkDevice                                    device,
    uint32_t                                    fenceCount,
    const VkFence*                              pFences);
HRESULT vkd3d_create_image_resource(
    ID3D12Device                              *device,
    const struct vkd3d_image_resource_create_info   *create_info, 
    ID3D12Resource                            **resource);
```
To be honest, too many functions are listed above which might make you confused. If you are not familiar with Vulkan, you can read the Vulkan tutorial to make acquaintance with it. Anyway, I will show a simple instance to you, and you can learn how to use the vkd3d functions.  
Here's how we do it: 
```c
//1. Get d3d12 device
ID3D12Device *d3d12_device;
ID3D12CommandQueue_GetDevice(cxt->command_queue, &IID_ID3D12Device, (void **)&d3d12_device);
//2. Get vulkan data with the help of vkd3d functions
//2.1 Get vk device by using vkd3d_get_vk_device()
VkDevice vk_device = vkd3d_get_vk_device(d3d12_device);
//2.2 Get vk physical device by using vkd3d_get_vk_physical_device()
vk_physical_device = vkd3d_get_vk_physical_device(d3d12_device);
//2.3 Get vk instance: first use vkd3d_instance_from_device() to get vkd3d_instance data, and then use vkd3d_instance_get_vk_instance() to get VkInstance data
VkInstance vk_instance = vkd3d_instance_get_vk_instance(vkd3d_instance_from_device(d3d12_device));
//2.4 get vk format
VkFormat format = vkd3d_get_vk_format(swapchain_desc.format);
//3. Set the basic information of swapchain 
struct VkSwapchainCreateInfoKHR vk_swapchain_desc;
//3.1 Get vulkan surface
struct VkXcbSurfaceCreateInfoKHR surface_desc;
surface_desc.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
surface_desc.pNext = NULL;
surface_desc.flags = 0;
surface_desc.connection = cxt->window->demo->connection;
surface_desc.window = cxt->window->window;
VkSurfaceKHR vk_surface;
vkCreateXcbSurfaceKHR(vk_instance, &surface_desc, NULL, &vk_surface);
//3.2 Get vk surface capabilities
VkSurfaceCapabilitiesKHR surface_caps;
vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vk_physical_device, vk_surface, &surface_caps);
//4. Set the information of vk_swapchain
VkSwapchainKHR vk_swapchain = VK_NULL_HANDLE;
vk_swapchain_desc.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
vk_swapchain_desc.pNext = NULL;
vk_swapchain_desc.flags = 0;
vk_swapchain_desc.surface = vk_surface;
vk_swapchain_desc.minImageCount = ARRAY_SIZE(cxt->render_targets);
vk_swapchain_desc.imageFormat = format;
vk_swapchain_desc.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
vk_swapchain_desc.imageExtent.width = cxt->width;
vk_swapchain_desc.imageExtent.height = cxt->height;
vk_swapchain_desc.imageArrayLayers = 1;
vk_swapchain_desc.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
vk_swapchain_desc.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
vk_swapchain_desc.queueFamilyIndexCount = 0;
vk_swapchain_desc.pQueueFamilyIndices = NULL;
vk_swapchain_desc.preTransform = surface_caps.currentTransform;
vk_swapchain_desc.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
vk_swapchain_desc.presentMode = VK_PRESENT_MODE_FIFO_KHR;
vk_swapchain_desc.clipped = VK_TRUE;
vk_swapchain_desc.oldSwapchain = VK_NULL_HANDLE;
//5. Create vulkan swapchain
vkCreateSwapchainKHR(vk_device, &vk_swapchain_desc, NULL, &vk_swapchain);
//6. Create image resource
//6.1 Get image count
vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &image_count, NULL);
//6.2 Get vk image
VkImage *vk_images = calloc(image_count, sizeof(*vk_images));
vkGetSwapchainImagesKHR(vk_device, vk_swapchain, &image_count, vk_images);
//6.3 Get the swaphain's current buffer
//6.3.1 Ceate vk fence
VkFence vk_fence = VK_NULL_HANDLE;
fence_desc.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
fence_desc.pNext = NULL;
fence_desc.flags = 0;
vkCreateFence(vk_device, &fence_desc, NULL, &vk_fence);
//6.3.2 Get current buffer
uint32_t current_buffer;
vkAcquireNextImageKHR(vk_device, vk_swapchain, UINT64_MAX,
            VK_NULL_HANDLE, vk_fence, &current_buffer);
//6.4 Set the information of the image resource
resource_create_info.type = VKD3D_STRUCTURE_TYPE_IMAGE_RESOURCE_CREATE_INFO;
resource_create_info.next = NULL;
resource_create_info.desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
resource_create_info.desc.Alignment = 0;
resource_create_info.desc.Width = cxt->width;
resource_create_info.desc.Height = cxt->height;
resource_create_info.desc.DepthOrArraySize = 1;
resource_create_info.desc.MipLevels = 1;
resource_create_info.desc.Format = desc->format;
resource_create_info.desc.SampleDesc.Count = 1;
resource_create_info.desc.SampleDesc.Quality = 0;
resource_create_info.desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
resource_create_info.desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
resource_create_info.flags = VKD3D_RESOURCE_INITIAL_STATE_TRANSITION | VKD3D_RESOURCE_PRESENT_STATE_TRANSITION;
resource_create_info.present_state = D3D12_RESOURCE_STATE_PRESENT;
//6.5 Create image resource and store it in buffers
ID3D12Resource *buffers[1];
for (i = 0; i < image_count; ++i)
{
    resource_create_info.vk_image = vk_images[i];
    vkd3d_create_image_resource(d3d12_device, &resource_create_info, &buffers[i]);
}
```
You can see the more detailed *demo_swapchain_create()* function in *demo/demo_xcb.h*. In that function, you could learn more about the listed functions, and use them with less difficulty.
### 2.3 Create Descriptor Heaps.
We can use *ID3D12Device_CreateDescriptorHeap()* to create the descriptor heaps in Linux.
```c
HRESULT ID3D12Device_CreateDescriptorHeap(
    ID3D12Device                              *This,
    const D3D12_DESCRIPTOR_HEAP_DESC          *desc,
    REFIID                                    riid,
    void                                      **descriptor_heap);
```
For example, we have to initialize the D3D12_DESCRIPTOR_HEAP_DESC data which would be used in the function ID3D12Device_CreateDescriptorHeap().
```c
D3D12_DESCRIPTOR_HEAP_DESC rtv_heap_desc;
memset(&rtv_heap_desc, 0, sizeof(rtv_heap_desc));
rtv_heap_desc.NumDescriptors = ARRAY_SIZE(cxt->render_targets);
rtv_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
rtv_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
HRESULT hr = ID3D12Device_CreateDescriptorHeap(cxt->device, &rtv_heap_desc,
            &IID_ID3D12DescriptorHeap, (void **)&cxt->rtv_heap);
```
### 2.4 Create Frame Resources
#### 2.4.1 Get the CPU descriptor handle
```c
D3D12_CPU_DESCRIPTOR_HANDLE ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(
    ID3D12DescriptorHeap                      *This);

D3D12_CPU_DESCRIPTOR_HANDLE rtv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxt->rtv_heap);    
```
#### 2.4.2 Get the back buffer of the swapchain 
Actually, there is no function used to get back buffer directly. We can use ID3D12Resource_AddRef() function to make it.
```c
ULONG ID3D12Resource_AddRef(ID3D12Resource* This);
```
I will show you a simple demo about how to get the back buffer. This demo is to get the swapchain buffer whose index is zero.
```c
ID3D12Resource *resource = cxt->swapchain->buffers[0];
ID3D12Resource_AddRef(resource);
cxt->render_targets[0] = resource;
```
#### 2.4.3 Create render target view
When you want to create a render-target view for accessing resource data, you can use the function called ID3D12Device_CreateRenderTargetView().
```c
void ID3D12Device_CreateRenderTargetView(
    ID3D12Device                              *This,
    ID3D12Resource                            *resource,
    const D3D12_RENDER_TARGET_VIEW_DESC       *desc,
    D3D12_CPU_DESCRIPTOR_HANDLE               descriptor);

ID3D12Device_CreateRenderTargetView(cxt->device, cxt->render_targets[0], NULL, rtv_handle);
rtv_handle.ptr += cxt->rtv_descriptor_size;
```
#### 2.4.4 Get descriptor handle increment size
When we want to get the size of the handle increment for the given type of descriptor heap, we can use ID3D12Device_GetDescriptorHandleIncrementSize().
```c
UINT ID3D12Device_GetDescriptorHandleIncrementSize(ID3D12Device* This,D3D12_DESCRIPTOR_HEAP_TYPE descriptor_heap_type);

cxt->rtv_descriptor_size = ID3D12Device_GetDescriptorHandleIncrementSize(cxt->device,
        D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
rtv_handle.ptr += cxt->rtv_descriptor_size;
```
#### 2.4.5 Create command allocator
When we try to create a command allocator object, ID3D12Device_CreateCommandAllocator() can be used.
```c
HRESULT ID3D12Device_CreateCommandAllocator(
    ID3D12Device                              *This,
    D3D12_COMMAND_LIST_TYPE                   type,
    REFIID                                    riid,
    void                                      **command_allocator);

HRESULT hr = ID3D12Device_CreateCommandAllocator(cxt->device, D3D12_COMMAND_LIST_TYPE_DIRECT,
            &IID_ID3D12CommandAllocator, (void **)&cxt->command_allocator);
```
## 3. Loading Assets
In fact, most functions in this chapter are derived from d3d12. Therefore, for those, I would just give examples, and for others, I would have more explanation about them. 
### 3.1 Create Root Signature
We can use *D3D12SerializeRootSignature()* and *ID3D12Device_CreateRootSignature()*. We need to *D3D12SerializeRootSignature()* first to get a pointer to a memory block that receives a pointer to the ID3DBlob interface that you can use to access the serialized root signature. And then *ID3D12Device_CreateRootSignature()* can help us create the root signature.
```c
HRESULT D3D12SerializeRootSignature(
    const D3D12_ROOT_SIGNATURE_DESC           *desc,
    D3D_ROOT_SIGNATURE_VERSION                version, 
    ID3DBlob                                  **blob, 
    ID3DBlob                                  **error_blob)
HRESULT ID3D12Device_CreateRootSignature(
    ID3D12Device                              *This,
    UINT                                      node_mask,
    const void                                *bytecode,
    SIZE_T                                    bytecode_length,
    REFIID                                    riid,
    void                                      **root_signature);
```
For example,
```c
HRESULT hr;
ID3DBlob *blob;
ID3D12RootSignature **signature = &cxt->root_signature;
//Get the ID3DBlob pointer blob
hr = D3D12SerializeRootSignature(&root_signature_desc, D3D_ROOT_SIGNATURE_VERSION_1, &blob, NULL);
//Create the root signature
hr = ID3D12Device_CreateRootSignature(cxt->device, 0, ID3D10Blob_GetBufferPointer(blob),
            ID3D10Blob_GetBufferSize(blob), &IID_ID3D12RootSignature, (void **)signature);
//Release the ID3DBlob pointer blob
ID3D10Blob_Release(blob);
```
### 3.2 Create Graphics Pipeline State
We can use *ID3D12Device_CreateGraphicsPipelineState()* to create the pipeline state.
```c
HRESULT ID3D12Device_CreateGraphicsPipelineState(
    ID3D12Device                              *This,
    const D3D12_GRAPHICS_PIPELINE_STATE_DESC  *desc,
    REFIID                                    riid,
    void                                      **pipeline_state);
```
For example,
```c
static inline void demo_rasterizer_desc_init_default(D3D12_RASTERIZER_DESC *desc)
{
    desc->FillMode = D3D12_FILL_MODE_SOLID;
    desc->CullMode = D3D12_CULL_MODE_BACK;
    desc->FrontCounterClockwise = FALSE;
    desc->DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
    desc->DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
    desc->SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
    desc->DepthClipEnable = TRUE;
    desc->MultisampleEnable = FALSE;
    desc->AntialiasedLineEnable = FALSE;
    desc->ForcedSampleCount = 0;
    desc->ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
}

static inline void demo_blend_desc_init_default(D3D12_BLEND_DESC *desc)
{
    static const D3D12_RENDER_TARGET_BLEND_DESC rt_blend_desc =
    {
        .BlendEnable = FALSE,
        .LogicOpEnable = FALSE,
        .SrcBlend = D3D12_BLEND_ONE,
        .DestBlend = D3D12_BLEND_ZERO,
        .BlendOp = D3D12_BLEND_OP_ADD,
        .SrcBlendAlpha = D3D12_BLEND_ONE,
        .DestBlendAlpha = D3D12_BLEND_ZERO,
        .BlendOpAlpha = D3D12_BLEND_OP_ADD,
        .LogicOp = D3D12_LOGIC_OP_NOOP,
        .RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL,
    };
    unsigned int i;

    desc->AlphaToCoverageEnable = FALSE;
    desc->IndependentBlendEnable = FALSE;
    for (i = 0; i < ARRAY_SIZE(desc->RenderTarget); ++i)
    {
        desc->RenderTarget[i] = rt_blend_desc;
    }
}
//Describes elements for the input-assembler stage of the graphics pipeline.
static const D3D12_INPUT_ELEMENT_DESC il_desc[] =
{
    {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
    {"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
};
//Describe a graphics pipeline state.
D3D12_GRAPHICS_PIPELINE_STATE_DESC pso_desc;
memset(&pso_desc, 0, sizeof(pso_desc));
//describe the data types of the input-assembler stage.
pso_desc.InputLayout.pInputElementDescs = il_desc;
pso_desc.InputLayout.NumElements = ARRAY_SIZE(il_desc);
pso_desc.pRootSignature = cxt->root_signature;
//VS---A D3D12_SHADER_BYTECODE structure that describes the vertex shader.
//VSMain is defined in your hlsl file.
pso_desc.VS.pShaderBytecode = VSMain;
pso_desc.VS.BytecodeLength = sizeof(VSMain);
//PS---A D3D12_SHADER_BYTECODE structure that describes the pixel shader.
//PSMain is defined in your hlsl file.
pso_desc.PS.pShaderBytecode = PSMain;
pso_desc.PS.BytecodeLength = sizeof(PSMain);
demo_rasterizer_desc_init_default(&pso_desc.RasterizerState);
demo_blend_desc_init_default(&pso_desc.BlendState);
pso_desc.DepthStencilState.DepthEnable = FALSE;
pso_desc.DepthStencilState.StencilEnable = FALSE;
pso_desc.SampleMask = UINT_MAX;
pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
pso_desc.NumRenderTargets = 1;
pso_desc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
pso_desc.SampleDesc.Count = 1;

//Create graphics pipeline state
HRESULT hr = ID3D12Device_CreateGraphicsPipelineState(cxt->device, &pso_desc,
            &IID_ID3D12PipelineState, (void **)&cxt->pipeline_state);
```
### 3.3 Create Command List
We can use *ID3D12Device_CreateCommandList()*
```c
HRESULT ID3D12Device_CreateCommandList(
    ID3D12Device                              *This,
    UINT                                      node_mask,
    D3D12_COMMAND_LIST_TYPE                   type,
    ID3D12CommandAllocator                    *command_allocator,
    ID3D12PipelineState                       *initial_pipeline_state,
    REFIID                                    riid,
    void                                      **command_list);
```
For example,
```c
HRESULT hr = ID3D12Device_CreateCommandList(cxt->device, 0, D3D12_COMMAND_LIST_TYPE_DIRECT, cxt->command_allocator,
            cxt->pipeline_state, &IID_ID3D12GraphicsCommandList, (void **)&cxt->command_list);
```
### 3.4 Create Committed Resource
We can use *ID3D12Device_CreateCommittedResource*
```c
HRESULT ID3D12Device_CreateCommittedResource(
    ID3D12Device                              *This,
    const D3D12_HEAP_PROPERTIES               *heap_properties,
    D3D12_HEAP_FLAGS                          heap_flags,
    const D3D12_RESOURCE_DESC                 *desc,
    D3D12_RESOURCE_STATES                     initial_state,
    const D3D12_CLEAR_VALUE                   *optimized_clear_value,
    REFIID                                    riid,
    void                                      **resource);
```
For example,
```c
//Set the value of heap_desc
heap_desc.Type = D3D12_HEAP_TYPE_UPLOAD;
heap_desc.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
heap_desc.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
heap_desc.CreationNodeMask = 1;
heap_desc.VisibleNodeMask = 1;
//Set the value of resource_desc
resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
resource_desc.Alignment = 0;
resource_desc.Width = sizeof(vertices);
resource_desc.Height = 1;
//DepthOrArraySize: Specifies the depth of the resource, if it is 3D, or the array size if it is an array of 1D or 2D resources.
resource_desc.DepthOrArraySize = 1; 
resource_desc.MipLevels = 1;
resource_desc.Format = DXGI_FORMAT_UNKNOWN;
resource_desc.SampleDesc.Count = 1;
resource_desc.SampleDesc.Quality = 0;
resource_desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;

//Create both a resource and an implicit heap, such that the heap is big enough to contain the entire resource, and the resource is mapped to the heap.
HRESULT hr = ID3D12Device_CreateCommittedResource(cxt->device, &heap_desc, D3D12_HEAP_FLAG_NONE, &resource_desc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, &IID_ID3D12Resource, (void **)&cxt->vb);
```
### 3.5 Map
We can use *ID3D12Resource_Map()*
```c
HRESULT ID3D12Resource_Map(
    ID3D12Resource                            *This,
    UINT                                      sub_resource,
    const D3D12_RANGE                         *read_range,
    void                                      **data);
```
For example,
```c
void *data_vb;
HRESULT hr = ID3D12Resource_Map(cxt->vb, 0, &read_range, &data_vb);
memcpy(data_vb, vertices, sizeof(vertices));
```
### 3.6 Get GPU Virtual Address
We can use *ID3D12Resource_GetGPUVirtualAddress()*
```c
D3D12_GPU_VIRTUAL_ADDRESS ID3D12Resource_GetGPUVirtualAddress(
    ID3D12Resource                            *This);
```
For example,
```c
//returns the GPU virtual address of a buffer resource.
cxt->vbv.BufferLocation = ID3D12Resource_GetGPUVirtualAddress(cxt->vb);
```
### 3.7 Create Fence
We can use *ID3D12Device_CreateFence()*
```c
HRESULT ID3D12Device_CreateFence(
    ID3D12Device                              *This,
    UINT64                                    initial_value,
    D3D12_FENCE_FLAGS                         flags,
    REFIID                                    riid,
    void                                      **fence)
```
For example,
```c
struct cxt_fence *fence = &cxt->fence;
HRESULT hr = ID3D12Device_CreateFence(cxt->device, 0, D3D12_FENCE_FLAG_NONE, &IID_ID3D12Fence, (void **)&fence->fence); 
```
### 3.8 Create Event
In Windows, CreateEvent()
```c
HANDLE WINAPI CreateEventW(
    LPSECURITY_ATTRIBUTES                     lpEventAttributes,
    BOOL                                      bManualReset,
    BOOL                                      bInitialState,
    LPCWSTR                                   lpName);
```
In vkd3d, vkd3d_create_event()
```c
HANDLE vkd3d_create_event(void);
```
For example,
```c
fence->event = vkd3d_create_event();
```
### 3.9 Wait for Previous Frame
We can use *ID3D12Fence_SetEventOnCompletion()* to specify an event that should be fired when the fence reaches a certain value.
```c
HRESULT ID3D12Fence_SetEventOnCompletion(
    ID3D12Fence                               *This,
    UINT64                                    value,
    HANDLE                                    event);
```
For example,
```c
struct cxr_fence *fence = &cxr->fence;
HRESULT hr = ID3D12Fence_SetEventOnCompletion(fence->fence, v, fence->event);
```
Also, in vkd3d, we can use vkd3d_wait_event() to wait the event.
```c
unsigned int vkd3d_wait_event(
    HANDLE                                    event, 
    unsigned int                              milliseconds);
```
For example,
```c
vkd3d_wait_event(fence->event, INFINITE);
```
## 4. Render Frame
### 4.1 Populate CommandList
We can use the functions below to populate the command list. All of the following functions are from d3d12, therefore, I would not explain anything about them. It is simple for us to use them in Linux.
```c
//reset command_allocator
HRESULT ID3D12CommandAllocator_Reset(ID3D12CommandAllocator* This);  
//reset command list 
HRESULT ID3D12GraphicsCommandList_Reset(
    ID3D12GraphicsCommandList                 *This,
    ID3D12CommandAllocator                    *allocator,
    ID3D12PipelineState                       *initial_state);

//Set necessary state.
void ID3D12GraphicsCommandList_SetGraphicsRootSignature(
    ID3D12GraphicsCommandList                 *This,
    ID3D12RootSignature                       *root_signature);
void ID3D12GraphicsCommandList_RSSetViewports(
    ID3D12GraphicsCommandList                 *This,
    UINT                                      viewport_count,
    const D3D12_VIEWPORT                      *viewports);
void ID3D12GraphicsCommandList_RSSetScissorRects(
    ID3D12GraphicsCommandList                 *This,
    UINT                                      rect_count,
    const D3D12_RECT                          *rects);
//Indicate that the back buffer will be used as a render target.
void ID3D12GraphicsCommandList_ResourceBarrier(
    ID3D12GraphicsCommandList                 *This,
    UINT                                      barrier_count,
    const D3D12_RESOURCE_BARRIER              *barriers);
void ID3D12GraphicsCommandList_OMSetRenderTargets(
    ID3D12GraphicsCommandList                 *This,
    UINT                                      render_target_descriptor_count,
    const D3D12_CPU_DESCRIPTOR_HANDLE         *render_target_descriptors,
    BOOL                                      single_descriptor_handle,
    const D3D12_CPU_DESCRIPTOR_HANDLE         *depth_stencil_descriptor);
```
For example,
```c
HRESULT hr;
hr = ID3D12CommandAllocator_Reset(cxt->command_allocator);
hr = ID3D12GraphicsCommandList_Reset(cxt->command_list, cxt->command_allocator, cxt->pipeline_state);

ID3D12GraphicsCommandList_SetGraphicsRootSignature(cxt->command_list, cxt->root_signature);
ID3D12GraphicsCommandList_RSSetViewports(cxt->command_list, 1, &cxt->vp);
ID3D12GraphicsCommandList_RSSetScissorRects(cxt->command_list, 1, &cxt->scissor_rect);

D3D12_RESOURCE_BARRIER barrier;
barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
barrier.Transition.pResource = cxt->render_targets[cxt->frame_idx];
barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
ID3D12GraphicsCommandList_ResourceBarrier(cxt->command_list, 1, &barrier);

rtv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxt->rtv_heap);
rtv_handle.ptr += cxt->frame_idx * cxt->rtv_descriptor_size;
ID3D12GraphicsCommandList_OMSetRenderTargets(cxt->command_list, 1, &rtv_handle, FALSE, NULL);
```
### 4.2 Record Commands  
```c
void ID3D12GraphicsCommandList_ClearRenderTargetView(
    ID3D12GraphicsCommandList                 *This,
    D3D12_CPU_DESCRIPTOR_HANDLE               rtv,
    const FLOAT                               color[4],
    UINT                                      rect_count,
    const D3D12_RECT                          *rects);
void ID3D12GraphicsCommandList_IASetPrimitiveTopology(
    ID3D12GraphicsCommandList                 *This,
    D3D12_PRIMITIVE_TOPOLOGY                  primitive_topology);
void ID3D12GraphicsCommandList_IASetVertexBuffers(
    ID3D12GraphicsCommandList                 *This,
    UINT                                      start_slot,
    UINT                                      view_count,
    const D3D12_VERTEX_BUFFER_VIEW            *views)
void ID3D12GraphicsCommandList_DrawInstanced(
    ID3D12GraphicsCommandList                 *This,
    UINT                                      vertex_count_per_instance,
    UINT                                      instance_count,
    UINT                                      start_vertex_location,
    UINT                                      start_instance_location);
```
For example, when we want to draw a triangle, we can use these functions like this.
```c
ID3D12GraphicsCommandList_ClearRenderTargetView(cxt->command_list, rtv_handle, clear_colour, 0, NULL);
ID3D12GraphicsCommandList_IASetPrimitiveTopology(cxt->command_list, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
ID3D12GraphicsCommandList_IASetVertexBuffers(cxt->command_list, 0, 1, &cxt->vbv);
ID3D12GraphicsCommandList_DrawInstanced(cxt->command_list, 3, 1, 0, 0);
```
### 4.3 Execute the Command List
We can use *ID3D12CommandQueue_ExecuteCommandLists()* to submit an array of command lists for execution.
```c
void ID3D12CommandQueue_ExecuteCommandLists(
    ID3D12CommandQueue                        *This,
    UINT                                      command_list_count,
    ID3D12CommandList *const                  *command_lists);
```
For example, 
```c
ID3D12CommandQueue_ExecuteCommandLists(cxt->command_queue, 1, (ID3D12CommandList **)&cxt->command_list);
```
### 4.4 Present the Swapchain.
In vkd3d, we can use these functions to present the swapchain. Most of them are derived from vulkan. You can have better understanding if you learn the basic use of vulkan.     

*vkd3d_acquire_vk_queue()* can help us get VKQueue data from the swapchain's command queue.
```c
VkQueue vkd3d_acquire_vk_queue(
    ID3D12CommandQueue                        *queue);

VkQueue vk_queue = vkd3d_acquire_vk_queue(cxt->swapchain->command_queue);
```
*vkQueuePresentKHR()* is used to queue an image for presentation.
```c
VKAPI_ATTR VkResult VKAPI_CALL vkQueuePresentKHR(
    VkQueue                                   queue,
    const VkPresentInfoKHR*                   pPresentInfo);
```
For example,
```c
VkPresentInfoKHR present_desc;

present_desc.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
present_desc.pNext = NULL;
present_desc.waitSemaphoreCount = 0;
present_desc.pWaitSemaphores = NULL;
present_desc.swapchainCount = 1;
present_desc.pSwapchains = &cxt->swapchain->vk_swapchain;
present_desc.pImageIndices = &cxt->swapchain->current_buffer;
present_desc.pResults = NULL;

vkQueuePresentKHR(vk_queue, &present_desc);
```
*vkd3d_release_vk_queue()* is used to release the command queue of the swapchain.
```c
void vkd3d_release_vk_queue(ID3D12CommandQueue *queue)l

vkd3d_release_vk_queue(cxt->swapchain->command_queue);
```
*vkAcquireNextImageKHR()* is to acquire an available presentable image to use, and retrieve the index of that image.
```c
VKAPI_ATTR VkResult VKAPI_CALL vkAcquireNextImageKHR(
    VkDevice                                  device,
    VkSwapchainKHR                            swapchain,
    uint64_t                                  timeout,
    VkSemaphore                               semaphore,
    VkFence                                   fence,
    uint32_t*                                 pImageIndex);

vkAcquireNextImageKHR(cxt->swapchain->vk_device, cxt->swapchain->vk_swapchain, UINT64_MAX,
            VK_NULL_HANDLE, cxt->swapchain->vk_fence, &cxt->swapchain->current_buffer);
```
*vkWaitForFences()* is to wait for one or more fences to enter the signaled state on the host.
```c
VKAPI_ATTR VkResult VKAPI_CALL vkWaitForFences(
    VkDevice                                  device,
    uint32_t                                  fenceCount,
    const VkFence*                            pFences,
    VkBool32                                  waitAll,
    uint64_t                                  timeout);

vkWaitForFences(swapchain->vk_device, 1, &swapchain->vk_fence, VK_TRUE, UINT64_MAX);
```
*vkResetFences()* can define a fence unsignal operation for each fence, which resets the fence to the unsignaled state.
```c
VKAPI_ATTR VkResult VKAPI_CALL vkResetFences(
    VkDevice                                  device,
    uint32_t                                  fenceCount,
    const VkFence*                            pFences);

vkResetFences(cxt->swapchain->vk_device, 1, &cxt->swapchain->vk_fence);
```
## 5.Process Events
### 5.1 Receiving events: writing the events loop
After we have registered for the event types we are interested in, we need to enter a loop of receiving events and handling them. There are two ways to receive events: a blocking way and a non-blocking way:
The blocking way:
```c
//Returns the next event or error from the server.
xcb_generic_event_t *xcb_wait_for_event(xcb_connection_t *c);
```
It blocks until an event is queued in the X server, then dequeues it from the queue, then returns it as a newly allocated structure (which is your responsibility to free). May return NULL in event of an error.    

The non-blocking way:
```c
//Returns the next event without reading from the connection.
xcb_generic_event_t *xcb_poll_for_event(xcb_connection_t *c);
```
It immediately dequeues and returns an event, but returns NULL if no event is available at the time of the call. If an error occurs, the parameter error will be filled with the error status.
### 5.2 Destroy window
```c
xcb_void_cookie_t xcb_destroy_window (
    xcb_connection_t                          *c,
    xcb_window_t                              window);

xcb_destroy_window(cxt->window->demo->connection, cxt->window->window);
```
## 6. Destroy Assets
### 6.1 Release Fence
```c
ULONG ID3D12Fence_Release(ID3D12Fence* This);

ID3D12Fence_Release(cxt_fence->fence);
```
### 6.2 Destroy Event
When trying to destroy the event, we can use vkd3d_destroy_event().
```c
void vkd3d_destroy_event(HANDLE event);

vkd3d_destroy_event(cxt_fence->event);
```
### 6.3 Release Resource
```c
ULONG ID3D12Resource_Release(ID3D12Resource* This);

//For instance, we have used the ID3D12Resource data *vb before, so we need to release it here.
ID3D12Resource_Release(cxt->vb);
```
### 6.4 Release Command List
```c
ULONG ID3D12GraphicsCommandList_Release(ID3D12GraphicsCommandList* This);

ID3D12GraphicsCommandList_Release(cxt->command_list);
```
### 6.5 Release Pipeline State
```c
ULONG ID3D12PipelineState_Release(ID3D12PipelineState* This);

ID3D12PipelineState_Release(cxt->pipeline_state);
```
### 6.6 Release Root signature
```c
ULONG ID3D12RootSignature_Release(ID3D12RootSignature* This);

ID3D12RootSignature_Release(cxt->root_signature);
```
## 7. Destroy Pipeline
### 7.1 Release Resource
When you want to release the resource, these functions below are what you need. 
```c
//release command allocator
ULONG ID3D12CommandAllocator_Release(ID3D12CommandAllocator* This);
//release resource
ULONG ID3D12Resource_Release(ID3D12Resource* This);
//release descriptor heap
ULONG ID3D12DescriptorHeap_Release(ID3D12DescriptorHeap* This);
//release command queue
ULONG ID3D12CommandQueue_Release(ID3D12CommandQueue* This);
//release device
ULONG ID3D12Device_Release(ID3D12Device* This);
```
I would give the examples of these functions.
```c
ID3D12CommandAllocator_Release(cxt->command_allocator);
for (i = 0; i < ARRAY_SIZE(cxt->render_targets); ++i)
{
    ID3D12Resource_Release(cxt->render_targets[i]);
}
ID3D12DescriptorHeap_Release(cxt->rtv_heap);
ID3D12CommandQueue_Release(cxt->command_queue);
ID3D12Device_Release(cxt->device);
```
### 7.2 Destroy Swapchain
```c
//destroy fence
VKAPI_ATTR void VKAPI_CALL vkDestroyFence(
    VkDevice                                    device,
    VkFence                                     fence,
    const VkAllocationCallbacks*                pAllocator);
//destroy swapchain KHR
VKAPI_ATTR void VKAPI_CALL vkDestroySwapchainKHR(
    VkDevice                                    device,
    VkSwapchainKHR                              swapchain,
    const VkAllocationCallbacks*                pAllocator);
//destroy surface KHR
VKAPI_ATTR void VKAPI_CALL vkDestroySurfaceKHR(
    VkInstance                                  instance,
    VkSurfaceKHR                                surface,
    const VkAllocationCallbacks*                pAllocator);
```
It's easier to us to know how to destroy the swapchain if we can remember the procedure to create the swapchain in the chapter 2.2. We need to release the command queue and buffer, destroy the vk fence, vk swapchain and vk surface, and then free the swapchain buffer. I would give an example about destroying the swapchain that we have created in chapter 2.2.
```c
ID3D12CommandQueue_Release(cxt->swapchain->command_queue);
for (unsigned int i = 0; i < swapchain->buffer_count; ++i)
{
    ID3D12Resource_Release(cxt->swapchain->buffers[i]);
}
vkDestroyFence(cxt->swapchain->vk_device, cxt->swapchain->vk_fence, NULL);
vkDestroySwapchainKHR(cxt->swapchain->vk_device, cxt->swapchain->vk_swapchain, NULL);
vkDestroySurfaceKHR(cxt->swapchain->vk_instance, cxt->swapchain->vk_surface, NULL);
free(cxt->swapchain);
```
## 8. Clean up  
We can use *xcb_key_symbols_free()* to free xcb_key_symbols_t and *xcb_disconnect()* to close the connection.
```c
void xcb_key_symbols_free(xcb_key_symbols_t *syms);

void xcb_disconnect(xcb_connection_t *c);
```
For example,
```c
xcb_key_symbols_free(cxt->demo->xcb_keysyms);

xcb_disconnect(cxt->demo->connection);
```