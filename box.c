/*
 * Copyright 2021 Qingyi Wang
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/*
 * This application contains code derived from BoxApp.cpp by Frank Luna (C) 
 * 2015 All Rights Reserved.
 *
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#define INITGUID
#define _GNU_SOURCE
#include <sys/time.h>
#include <assert.h>
#include <stdio.h>
#include <math.h>
#include "demo.h"

#include "box_vs.h"
#include "box_ps.h"

struct cxb_fence
{
    ID3D12Fence *fence;
    UINT64 value;
    HANDLE event;
};

struct cxb_cb_data
{
    float gWorldViewProj[16];
};

struct cx_box
{
    struct demo demo;

    struct demo_window *window;

    unsigned int width;
    unsigned int height;
    float aspect_ratio;

    float theta;
    float phi;

    D3D12_VIEWPORT vp;
    D3D12_RECT scissor_rect;

    ID3D12Device *device;
    ID3D12CommandQueue *command_queue;
    struct demo_swapchain *swapchain;
    ID3D12DescriptorHeap *rtv_heap, *dsv_heap;
    unsigned int rtv_descriptor_size;
    ID3D12Resource *render_targets[3];
    ID3D12CommandAllocator *command_allocator;

    ID3D12RootSignature *root_signature;
    ID3D12PipelineState *pipeline_state;
    ID3D12GraphicsCommandList *command_list;
    ID3D12Resource *ds, *cb, *vb, *ib;
    D3D12_VERTEX_BUFFER_VIEW vbv;
    D3D12_INDEX_BUFFER_VIEW ibv;
    unsigned int frame_idx;
    struct cxb_fence fence;
    struct cxb_cb_data *cb_data;

    float world[16];
    float view[16];
    float proj[16];
};

static void cxb_populate_command_list(struct cx_box *cxb)
{
    static const float clear_colour[] = {0.0f, 0.2f, 0.4f, 1.0f};

    D3D12_CPU_DESCRIPTOR_HANDLE rtv_handle, dsv_handle;
    D3D12_RESOURCE_BARRIER barrier;
    HRESULT hr;

    hr = ID3D12CommandAllocator_Reset(cxb->command_allocator);
    assert(SUCCEEDED(hr));

    hr = ID3D12GraphicsCommandList_Reset(cxb->command_list, cxb->command_allocator, cxb->pipeline_state);
    assert(SUCCEEDED(hr));

    ID3D12GraphicsCommandList_SetGraphicsRootSignature(cxb->command_list, cxb->root_signature);
    ID3D12GraphicsCommandList_SetGraphicsRootConstantBufferView(cxb->command_list, 0,
            ID3D12Resource_GetGPUVirtualAddress(cxb->cb));

    ID3D12GraphicsCommandList_RSSetViewports(cxb->command_list, 1, &cxb->vp);
    ID3D12GraphicsCommandList_RSSetScissorRects(cxb->command_list, 1, &cxb->scissor_rect);

    barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    barrier.Transition.pResource = cxb->render_targets[cxb->frame_idx];
    barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
    barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
    ID3D12GraphicsCommandList_ResourceBarrier(cxb->command_list, 1, &barrier);

    rtv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxb->rtv_heap);
    rtv_handle.ptr += cxb->frame_idx * cxb->rtv_descriptor_size;
    dsv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxb->dsv_heap);
    ID3D12GraphicsCommandList_OMSetRenderTargets(cxb->command_list, 1, &rtv_handle, FALSE, &dsv_handle);

    ID3D12GraphicsCommandList_ClearRenderTargetView(cxb->command_list, rtv_handle, clear_colour, 0, NULL);
    ID3D12GraphicsCommandList_ClearDepthStencilView(cxb->command_list,
            dsv_handle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);

    ID3D12GraphicsCommandList_IASetPrimitiveTopology(cxb->command_list, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    ID3D12GraphicsCommandList_IASetVertexBuffers(cxb->command_list, 0, 1, &cxb->vbv);
    ID3D12GraphicsCommandList_IASetIndexBuffer(cxb->command_list, &cxb->ibv);
    ID3D12GraphicsCommandList_DrawIndexedInstanced(cxb->command_list, 36, 1, 0, 0, 0);
    ID3D12GraphicsCommandList_SetPipelineState(cxb->command_list, cxb->pipeline_state);

    barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
    barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
    ID3D12GraphicsCommandList_ResourceBarrier(cxb->command_list, 1, &barrier);

    hr = ID3D12GraphicsCommandList_Close(cxb->command_list);
    assert(SUCCEEDED(hr));
}

static void cxb_wait_for_previous_frame(struct cx_box *cxb)
{
    struct cxb_fence *fence = &cxb->fence;
    const UINT64 v = fence->value;
    HRESULT hr;

    hr = ID3D12CommandQueue_Signal(cxb->command_queue, fence->fence, v);
    assert(SUCCEEDED(hr));

    ++fence->value;

    if (ID3D12Fence_GetCompletedValue(fence->fence) < v)
    {
        hr = ID3D12Fence_SetEventOnCompletion(fence->fence, v, fence->event);
        assert(SUCCEEDED(hr));
        demo_wait_event(fence->event, INFINITE);
    }

    cxb->frame_idx = demo_swapchain_get_current_back_buffer_index(cxb->swapchain);
}

static void matrix_multiply(float *b,float* a, float *res)
{
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            res[i * 4 + j] = a[j] * b[i * 4]
                    + a[j +  4] * b[i * 4 + 1]
                    + a[j +  8] * b[i * 4 + 2]
                    + a[j + 12] * b[i * 4 + 3];
        }
    }
}

static void demo_vec4_minus(struct demo_vec4 *res, struct demo_vec4 a, struct demo_vec4 b)
{
    res->x = a.x - b.x;
    res->y = a.y - b.y;
    res->z = a.z - b.z;
    res->w = a.w - b.w;
}

static void normal(struct demo_vec4 *res, struct demo_vec4 a)
{
    res->w = a.w;
    float s = sqrt(a.x*a.x+a.y*a.y+a.z*a.z);
    res->x = a.x/s;
    res->y = a.y/s;
    res->z = a.z/s;
}

static void cross(struct demo_vec4 *res, struct demo_vec4 a, struct demo_vec4 b)
{
    res->w = 1.0f;
    res->x = a.y*b.z-a.z*b.y;
    res->y = a.z*b.x-a.x*b.z;
    res->z = a.x*b.y-a.y*b.x;
}

static float dot(struct demo_vec4 a, struct demo_vec4 b)
{
    float res = a.x*b.x+a.y*b.y+a.z*b.z;
    return res;
}

static void transpose(float* res, float* a)
{
    for(int i=0; i<4; i++)
        for(int j=0; j<4; j++)
        {
            res[4*j+i] = a[4*i+j];
        }
}

static void update_projection(float* proj, float FovAngleY, float AspectRatio, float NearZ, float FarZ)
{
    float SinFov = sin(0.5f*FovAngleY);
    float CosFov = cos(0.5f*FovAngleY);
    float Height = CosFov / SinFov;
    float Width = Height / AspectRatio;
    float fRange = FarZ / (FarZ-NearZ);

    proj[0] = Width; proj[1] = 0.0f;   proj[2] = 0.0f;    proj[3] = 0.0f;
    proj[4] = 0.0f;  proj[5] = Height; proj[6] = 0.0f;    proj[7] = 0.0f;
    proj[8] = 0.0f;  proj[9] = 0.0f;   proj[10] = fRange; proj[11] = 1.0f;
    proj[12] = 0.0f; proj[13] = 0.0f;  proj[14] = -fRange * NearZ; proj[15] = 0.0f;
}

static void cxb_update_mvp(struct cx_box *cxb)
{
    float mRadius = 3.0f;
    // Convert Spherical to Cartesian coordinates.
    float x = mRadius*sinf(cxb->phi)*cosf(cxb->theta);
    float z = mRadius*sinf(cxb->phi)*sinf(cxb->theta);
    float y = mRadius*cosf(cxb->phi);

    // Build the view matrix.
    struct demo_vec4 pos, target, up;
    
    demo_vec4_set(&pos, x, y, z, 1.0f);
    demo_vec4_set(&target, 0.0f, 0.0f, 0.0f, 0.0f);
    demo_vec4_set(&up, 0.0f, 1.0f, 0.0f, 0.0f);

    //float view[] = XMMatrixLookAtLH(pos, target, up);
    struct demo_vec4 xaxis, yaxis, zaxis, temp;
    demo_vec4_set(&temp, 0.0f, 0.0f, 0.0f, 0.0f);
    demo_vec4_minus(&temp, target, pos);
    normal(&zaxis, temp);
    cross(&temp, up, zaxis);
    normal(&xaxis, temp);
    cross(&yaxis, zaxis, xaxis);

    cxb->view[0] = xaxis.x; cxb->view[4] = xaxis.y; cxb->view[8] = xaxis.z; 
    cxb->view[1] = yaxis.x; cxb->view[5] = yaxis.y; cxb->view[9] = yaxis.z; 
    cxb->view[2] = zaxis.x; cxb->view[6] = zaxis.y; cxb->view[10] = zaxis.z; 
    cxb->view[12] = -dot(xaxis, pos);
    cxb->view[13] = -dot(yaxis, pos);
    cxb->view[14] = -dot(zaxis, pos);
    cxb->view[15] = 1.0f;

    update_projection(cxb->proj, 0.25f*M_PI, cxb->aspect_ratio, 0.1f, 8.0f);

    float world_view[16];
    memset(&world_view, 0, sizeof(world_view));
    float world_view_proj[16];
    memset(&world_view_proj, 0, sizeof(world_view_proj));

    matrix_multiply(cxb->world, cxb->view, world_view);
    matrix_multiply(world_view, cxb->proj, world_view_proj);
    transpose(cxb->cb_data->gWorldViewProj, world_view_proj);
}

static void cxb_render_frame(struct cx_box *cxb)
{   
    cxb_populate_command_list(cxb);
    ID3D12CommandQueue_ExecuteCommandLists(cxb->command_queue, 1, (ID3D12CommandList **)&cxb->command_list);
    demo_swapchain_present(cxb->swapchain);
    cxb_wait_for_previous_frame(cxb);
}

static void cxb_destroy_pipeline(struct cx_box *cxb)
{
    unsigned int i;

    ID3D12CommandAllocator_Release(cxb->command_allocator);
    for (i = 0; i < ARRAY_SIZE(cxb->render_targets); ++i)
    {
        ID3D12Resource_Release(cxb->render_targets[i]);
    }
    ID3D12DescriptorHeap_Release(cxb->dsv_heap);
    ID3D12DescriptorHeap_Release(cxb->rtv_heap);
    demo_swapchain_destroy(cxb->swapchain);
    ID3D12CommandQueue_Release(cxb->command_queue);
    ID3D12Device_Release(cxb->device);
}

static void cxb_load_pipeline(struct cx_box *cxb)
{
    struct demo_swapchain_desc swapchain_desc;
    D3D12_DESCRIPTOR_HEAP_DESC rtv_heap_desc;
    D3D12_CPU_DESCRIPTOR_HANDLE rtv_handle;
    D3D12_COMMAND_QUEUE_DESC queue_desc;
    unsigned int i;
    HRESULT hr;

    hr = D3D12CreateDevice(NULL, D3D_FEATURE_LEVEL_11_0, &IID_ID3D12Device, (void **)&cxb->device);
    assert(SUCCEEDED(hr));

    memset(&queue_desc, 0, sizeof(queue_desc));
    queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
    hr = ID3D12Device_CreateCommandQueue(cxb->device, &queue_desc,
            &IID_ID3D12CommandQueue, (void **)&cxb->command_queue);
    assert(SUCCEEDED(hr));

    swapchain_desc.buffer_count = ARRAY_SIZE(cxb->render_targets);
    swapchain_desc.format = DXGI_FORMAT_B8G8R8A8_UNORM;
    swapchain_desc.width = cxb->width;
    swapchain_desc.height = cxb->height;
    cxb->swapchain = demo_swapchain_create(cxb->command_queue, cxb->window, &swapchain_desc);
    assert(cxb->swapchain);
    cxb->frame_idx = demo_swapchain_get_current_back_buffer_index(cxb->swapchain);

    memset(&rtv_heap_desc, 0, sizeof(rtv_heap_desc));
    rtv_heap_desc.NumDescriptors = ARRAY_SIZE(cxb->render_targets);
    rtv_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
    rtv_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    hr = ID3D12Device_CreateDescriptorHeap(cxb->device, &rtv_heap_desc,
            &IID_ID3D12DescriptorHeap, (void **)&cxb->rtv_heap);
    assert(SUCCEEDED(hr));

    cxb->rtv_descriptor_size = ID3D12Device_GetDescriptorHandleIncrementSize(cxb->device,
            D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    rtv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxb->rtv_heap);
    for (i = 0; i < ARRAY_SIZE(cxb->render_targets); ++i)
    {
        cxb->render_targets[i] = demo_swapchain_get_back_buffer(cxb->swapchain, i);
        ID3D12Device_CreateRenderTargetView(cxb->device, cxb->render_targets[i], NULL, rtv_handle);
        rtv_handle.ptr += cxb->rtv_descriptor_size;
    }
    rtv_heap_desc.NumDescriptors = 1;
    rtv_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
    rtv_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    hr = ID3D12Device_CreateDescriptorHeap(cxb->device, &rtv_heap_desc,
            &IID_ID3D12DescriptorHeap, (void **)&cxb->dsv_heap);
    assert(SUCCEEDED(hr));   

    hr = ID3D12Device_CreateCommandAllocator(cxb->device, D3D12_COMMAND_LIST_TYPE_DIRECT,
            &IID_ID3D12CommandAllocator, (void **)&cxb->command_allocator);
    assert(SUCCEEDED(hr));
}

static void cxb_fence_destroy(struct cxb_fence *cxb_fence)
{
    ID3D12Fence_Release(cxb_fence->fence);
    demo_destroy_event(cxb_fence->event);
}

static void cxb_destroy_assets(struct cx_box *cxb)
{
    cxb_fence_destroy(&cxb->fence);
    ID3D12Resource_Release(cxb->ib);
    ID3D12Resource_Release(cxb->vb);
    ID3D12Resource_Unmap(cxb->cb, 0, NULL);
    ID3D12Resource_Release(cxb->cb);
    ID3D12Resource_Release(cxb->ds);
    ID3D12GraphicsCommandList_Release(cxb->command_list);
    ID3D12PipelineState_Release(cxb->pipeline_state);
    ID3D12RootSignature_Release(cxb->root_signature);
}

static void cxb_fence_create(struct cxb_fence *fence, ID3D12Device *device)
{
    HRESULT hr;

    hr = ID3D12Device_CreateFence(device, 0, D3D12_FENCE_FLAG_NONE,
            &IID_ID3D12Fence, (void **)&fence->fence);
    assert(SUCCEEDED(hr));
    fence->value = 1;
    fence->event = demo_create_event();
    assert(fence->event);
}

static void cxb_load_assets(struct cx_box *cxb)
{
    static const D3D12_INPUT_ELEMENT_DESC il_desc[] =
    {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
        {"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
    };

    const struct
    {
        struct demo_vec3 position;
        struct demo_vec4 colour;
    }
    vertices[] =
    {
        {{ -0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, 0.0f, 1.0f}},//0
		{{ -0.5f, +0.5f, -0.5f}, {0.8f, 0.2f, 0.9f, 1.0f}},//1
		{{ +0.5f, +0.5f, -0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},//2
		{{ +0.5f, -0.5f, -0.5},  {0.0f, 1.0f, 0.0f, 1.0f}},//3
		{{ -0.5f, -0.5f, +0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},//4
		{{ -0.5f, +0.5f, +0.5f}, {0.8f, 0.8f, 0.0f, 1.0f}},//5
		{{ +0.5f, +0.5f, +0.5f}, {0.0f, 0.8f, 0.8f, 1.0f}},//6
		{{ +0.5f, -0.5f, +0.5f}, {0.8f, 0.0f, 0.8f, 1.0f}} //7
    };
    //define the sequence of vertices
    uint32_t indices[] = {
        // front face
		0, 1, 2,
		0, 2, 3,

		// back face
		4, 6, 5,
		4, 7, 6,

		// left face
		4, 5, 1,
		4, 1, 0,

		// right face
		3, 2, 6,
		3, 6, 7,

		// top face
		1, 5, 6,
		1, 6, 2,

		// bottom face
		4, 0, 3,
		4, 3, 7
    };
    D3D12_ROOT_SIGNATURE_DESC root_signature_desc;
    D3D12_ROOT_PARAMETER root_parameter;
    D3D12_GRAPHICS_PIPELINE_STATE_DESC pso_desc;
    D3D12_CPU_DESCRIPTOR_HANDLE dsv_handle;
    D3D12_RESOURCE_DESC resource_desc;
    D3D12_HEAP_PROPERTIES heap_desc;
    D3D12_RANGE read_range = {0, 0};
    D3D12_CLEAR_VALUE clear_value;
    HRESULT hr;
    void *data_ib;
    void *data_vb;

    root_parameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
    root_parameter.Descriptor.ShaderRegister = 0;
    root_parameter.Descriptor.RegisterSpace = 0;
    root_parameter.ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;

    memset(&root_signature_desc, 0, sizeof(root_signature_desc));
    root_signature_desc.NumParameters = 1;
    root_signature_desc.pParameters = &root_parameter;
    root_signature_desc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT
            | D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS
            | D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS
            | D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS
            | D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;
    hr = demo_create_root_signature(cxb->device, &root_signature_desc, &cxb->root_signature);
    assert(SUCCEEDED(hr));

    memset(&pso_desc, 0, sizeof(pso_desc));
    pso_desc.InputLayout.pInputElementDescs = il_desc;
    pso_desc.InputLayout.NumElements = ARRAY_SIZE(il_desc);
    pso_desc.pRootSignature = cxb->root_signature;
    pso_desc.VS.pShaderBytecode = g_VS;
    pso_desc.VS.BytecodeLength = sizeof(g_VS);
    pso_desc.PS.pShaderBytecode = g_PS;
    pso_desc.PS.BytecodeLength = sizeof(g_PS);

    demo_rasterizer_desc_init_default(&pso_desc.RasterizerState);
    pso_desc.RasterizerState.FrontCounterClockwise = FALSE;
    demo_blend_desc_init_default(&pso_desc.BlendState);
    pso_desc.DepthStencilState.DepthEnable = TRUE;
    pso_desc.DepthStencilState.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
    pso_desc.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
    pso_desc.DepthStencilState.StencilEnable = FALSE;
    pso_desc.SampleMask = UINT_MAX;
    pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    pso_desc.NumRenderTargets = 1;
    pso_desc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
    pso_desc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
    pso_desc.SampleDesc.Count = 1;
    hr = ID3D12Device_CreateGraphicsPipelineState(cxb->device, &pso_desc,
            &IID_ID3D12PipelineState, (void **)&cxb->pipeline_state);
    assert(SUCCEEDED(hr));

    hr = ID3D12Device_CreateCommandList(cxb->device, 0, D3D12_COMMAND_LIST_TYPE_DIRECT, cxb->command_allocator,
            cxb->pipeline_state, &IID_ID3D12GraphicsCommandList, (void **)&cxb->command_list);
    assert(SUCCEEDED(hr));

    hr = ID3D12GraphicsCommandList_Close(cxb->command_list);
    assert(SUCCEEDED(hr));

    heap_desc.Type = D3D12_HEAP_TYPE_DEFAULT;
    heap_desc.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    heap_desc.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    heap_desc.CreationNodeMask = 1;
    heap_desc.VisibleNodeMask = 1;

    resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    resource_desc.Alignment = 0;
    resource_desc.Width = cxb->width;
    resource_desc.Height = cxb->height;
    resource_desc.DepthOrArraySize = 1;
    resource_desc.MipLevels = 1;
    resource_desc.Format = DXGI_FORMAT_D32_FLOAT;
    resource_desc.SampleDesc.Count = 1;
    resource_desc.SampleDesc.Quality = 0;
    resource_desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    resource_desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

    clear_value.Format = DXGI_FORMAT_D32_FLOAT;
    clear_value.DepthStencil.Depth = 1.0f;
    clear_value.DepthStencil.Stencil = 0;

    hr = ID3D12Device_CreateCommittedResource(cxb->device, &heap_desc, D3D12_HEAP_FLAG_NONE, &resource_desc,
            D3D12_RESOURCE_STATE_DEPTH_WRITE, &clear_value, &IID_ID3D12Resource, (void **)&cxb->ds);
    assert(SUCCEEDED(hr));

    dsv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxb->dsv_heap);
    ID3D12Device_CreateDepthStencilView(cxb->device, cxb->ds, NULL, dsv_handle);

    heap_desc.Type = D3D12_HEAP_TYPE_UPLOAD;

    resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    resource_desc.Width = sizeof(*cxb->cb_data);
    resource_desc.Height = 1;
    resource_desc.Format = DXGI_FORMAT_UNKNOWN;
    resource_desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;

    hr = ID3D12Device_CreateCommittedResource(cxb->device, &heap_desc, D3D12_HEAP_FLAG_NONE, &resource_desc,
            D3D12_RESOURCE_STATE_GENERIC_READ, NULL, &IID_ID3D12Resource, (void **)&cxb->cb);
    assert(SUCCEEDED(hr));

    hr = ID3D12Resource_Map(cxb->cb, 0, &read_range, (void **)&cxb->cb_data);
    assert(SUCCEEDED(hr));
    cxb_update_mvp(cxb);

    resource_desc.Width = sizeof(vertices);
    hr = ID3D12Device_CreateCommittedResource(cxb->device, &heap_desc, D3D12_HEAP_FLAG_NONE, &resource_desc,
            D3D12_RESOURCE_STATE_GENERIC_READ, NULL, &IID_ID3D12Resource, (void **)&cxb->vb);
    assert(SUCCEEDED(hr));

    resource_desc.Width = sizeof(indices);
    hr = ID3D12Device_CreateCommittedResource(cxb->device, &heap_desc, D3D12_HEAP_FLAG_NONE, &resource_desc,
            D3D12_RESOURCE_STATE_GENERIC_READ, NULL, &IID_ID3D12Resource, (void **)&cxb->ib);
    assert(SUCCEEDED(hr));

    hr = ID3D12Resource_Map(cxb->vb, 0, &read_range, &data_vb);
    assert(SUCCEEDED(hr));
    memcpy(data_vb, vertices, sizeof(vertices));

    hr = ID3D12Resource_Map(cxb->ib, 0, &read_range, &data_ib);
    assert(SUCCEEDED(hr));
    memcpy(data_ib, indices, sizeof(indices));

    ID3D12Resource_Unmap(cxb->ib, 0, NULL);
    ID3D12Resource_Unmap(cxb->vb, 0, NULL);

    cxb->vbv.BufferLocation = ID3D12Resource_GetGPUVirtualAddress(cxb->vb);
    cxb->vbv.StrideInBytes = sizeof(*vertices);
    cxb->vbv.SizeInBytes = sizeof(vertices);

    cxb->ibv.BufferLocation = ID3D12Resource_GetGPUVirtualAddress(cxb->ib);
    cxb->ibv.SizeInBytes = sizeof(indices);
    cxb->ibv.Format = DXGI_FORMAT_R32_UINT;

    cxb_fence_create(&cxb->fence, cxb->device);
    cxb_wait_for_previous_frame(cxb);
}

static void cxb_key_press(struct demo_window *window, demo_key key, void *user_data)
{
    struct cx_box *cxb = user_data;

    switch (key)
    {
        case DEMO_KEY_ESCAPE:
            demo_window_destroy(window);
            break;
        case DEMO_KEY_LEFT:
            cxb->theta += M_PI / 36.0f;
            cxb_update_mvp(cxb);
            break;
        case DEMO_KEY_RIGHT:
            cxb->theta -= M_PI / 36.0f;
            cxb_update_mvp(cxb);
            break;
        case DEMO_KEY_UP:
            cxb->phi += M_PI / 36.0f;
            cxb_update_mvp(cxb);
            break;
        case DEMO_KEY_DOWN:
            cxb->phi -= M_PI / 36.0f;
            cxb_update_mvp(cxb);
            break;
        default:
            break;
    }

}

static void cxb_expose(struct demo_window *window, void *user_data)
{
    cxb_render_frame(user_data);
}

static void cxb_idle(struct demo *demo, void *user_data)
{
    cxb_render_frame(user_data);
}

static int cxb_main(void)
{
    unsigned int width = 640, height = 480;
    struct cx_box cxb;

    memset(&cxb, 0, sizeof(cxb));
    memset(&cxb.world, 0, sizeof(cxb.world));
    memset(&cxb.view, 0, sizeof(cxb.view));
    memset(&cxb.proj, 0, sizeof(cxb.proj));

    cxb.world[0] = cxb.world[5] = cxb.world[10] = cxb.world[15] = 1.0f;
    cxb.view[0] = cxb.view[5] = cxb.view[10] = cxb.view[15] = 1.0f;
    cxb.proj[0] = cxb.proj[5] = cxb.proj[10] = cxb.proj[15] = 1.0f;

    if (!demo_init(&cxb.demo, &cxb))
        return EXIT_FAILURE;
    demo_set_idle_func(&cxb.demo, cxb_idle);


    cxb.window = demo_window_create(&cxb.demo, "Vkd3d Box", width, height, &cxb);
    demo_window_set_key_press_func(cxb.window, cxb_key_press);
    demo_window_set_expose_func(cxb.window, cxb_expose);

    cxb.width = width;
    cxb.height = height;
    cxb.aspect_ratio = (float)width / (float)height;

    cxb.theta = M_PI / 6.0f;
    cxb.phi = M_PI / 9.0f;

    cxb.vp.Width = (float)width;
    cxb.vp.Height = (float)height;
    cxb.vp.MaxDepth = 1.0f;

    cxb.scissor_rect.right = width;
    cxb.scissor_rect.bottom = height;

    cxb_load_pipeline(&cxb);
    cxb_load_assets(&cxb);

    demo_process_events(&cxb.demo);

    cxb_wait_for_previous_frame(&cxb);
    cxb_destroy_assets(&cxb);
    cxb_destroy_pipeline(&cxb);
    demo_cleanup(&cxb.demo);

    return EXIT_SUCCESS;
}

#ifdef _WIN32
int wmain(void)
#else
int main(void)
#endif
{
    return cxb_main();
}
