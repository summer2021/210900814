/*
 * Copyright 2021 Qingyi Wang
 * 
 * This application contains code derived from Microsoft's "HelloTriangle"
 * demo and Vkd3d's "triangle" demo.
 * 
 */

/*
 * Copyright 2016 Henri Verbeet for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

/*
 * Microsoft's "HelloTriangle" license follows:
 *
 * Copyright (c) 2015 Microsoft
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#define INITGUID
#include <assert.h>
#include "demo.h"

#include "circle_vs.h"
#include "circle_ps.h"

struct cxr_fence
{
    ID3D12Fence *fence;
    UINT64 value;
    HANDLE event;
};

struct cx_rectangle
{
    struct demo demo;

    struct demo_window *window;

    unsigned int width;
    unsigned int height;
    float aspect_ratio;

    //Describes the dimensions of a viewport
    D3D12_VIEWPORT vp;
    //The RECT structure defines the coordinates of the upper-left and lower-right corners of a rectangle.
    D3D12_RECT scissor_rect;

    //Represents a virtual adapter; it is used to create command allocators, command lists, command queues, fences, resources, pipeline state objects, heaps, root signatures, samplers, and many resource views.
    ID3D12Device *device;
    //Provides methods for submitting command lists, synchronizing command list execution, instrumenting the command queue, and updating resource tile mappings.
    ID3D12CommandQueue *command_queue;
    //define swapchain using vulkan
    struct demo_swapchain *swapchain;
    //A descriptor heap is a collection of contiguous allocations of descriptors, one allocation for every descriptor.
    ID3D12DescriptorHeap *rtv_heap;
    //define rtv_descriptor size
    unsigned int rtv_descriptor_size;
    //Encapsulates a generalized ability of the CPU and GPU to read and write to physical memory, or heaps
    ID3D12Resource *render_targets[4];
    //Represents the allocations of storage for graphics processing unit (GPU) commands.
    ID3D12CommandAllocator *command_allocator;

    //The root signature defines what resources are bound to the graphics pipeline.
    ID3D12RootSignature *root_signature;
    //Represents the state of all currently set shaders as well as certain fixed function state objects.
    ID3D12PipelineState *pipeline_state;
    //Encapsulates a list of graphics commands for rendering.
    ID3D12GraphicsCommandList *command_list;
    //Encapsulates a generalized ability of the CPU and GPU to read and write to physical memory, or heaps
    ID3D12Resource *vb, *ib;
    //Describes a vertex buffer view.
    D3D12_VERTEX_BUFFER_VIEW vbv;
    //Describes the index buffer to view.
    D3D12_INDEX_BUFFER_VIEW ibv;

    unsigned int frame_idx;
    struct cxr_fence fence;
};

static void cxr_populate_command_list(struct cx_rectangle *cxr)
{
    static const float clear_colour[] = {0.0f, 0.0f, 0.0f, 1.0f};

    D3D12_CPU_DESCRIPTOR_HANDLE rtv_handle;
    D3D12_RESOURCE_BARRIER barrier;
    HRESULT hr;


    hr = ID3D12CommandAllocator_Reset(cxr->command_allocator);
    assert(SUCCEEDED(hr));

    hr = ID3D12GraphicsCommandList_Reset(cxr->command_list, cxr->command_allocator, cxr->pipeline_state);
    assert(SUCCEEDED(hr));

    ID3D12GraphicsCommandList_SetGraphicsRootSignature(cxr->command_list, cxr->root_signature);
    ID3D12GraphicsCommandList_RSSetViewports(cxr->command_list, 1, &cxr->vp);
    ID3D12GraphicsCommandList_RSSetScissorRects(cxr->command_list, 1, &cxr->scissor_rect);

    barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
    barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
    barrier.Transition.pResource = cxr->render_targets[cxr->frame_idx];
    barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
    barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
    barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
    ID3D12GraphicsCommandList_ResourceBarrier(cxr->command_list, 1, &barrier);

    rtv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxr->rtv_heap);
    rtv_handle.ptr += cxr->frame_idx * cxr->rtv_descriptor_size;
    ID3D12GraphicsCommandList_OMSetRenderTargets(cxr->command_list, 1, &rtv_handle, FALSE, NULL);

    ID3D12GraphicsCommandList_ClearRenderTargetView(cxr->command_list, rtv_handle, clear_colour, 0, NULL);
    ID3D12GraphicsCommandList_IASetPrimitiveTopology(cxr->command_list, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    ID3D12GraphicsCommandList_IASetVertexBuffers(cxr->command_list, 0, 1, &cxr->vbv);
    ID3D12GraphicsCommandList_IASetIndexBuffer(cxr->command_list, &cxr->ibv);
    //ID3D12GraphicsCommandList_DrawInstanced(cxr->command_list, 4, 2, 0, 0);//vertex 4
    //used for indices---6:numbers of indexes per instance 1:numbers of instances
    ID3D12GraphicsCommandList_DrawIndexedInstanced(cxr->command_list, 6, 1, 0, 0, 0); 

    barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
    barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
    ID3D12GraphicsCommandList_ResourceBarrier(cxr->command_list, 1, &barrier);

    hr = ID3D12GraphicsCommandList_Close(cxr->command_list);
    assert(SUCCEEDED(hr));
}

static void cxr_wait_for_previous_frame(struct cx_rectangle *cxr)
{
    struct cxr_fence *fence = &cxr->fence;
    const UINT64 v = fence->value;
    HRESULT hr;

    //Updates a fence to a specified value
    hr = ID3D12CommandQueue_Signal(cxr->command_queue, fence->fence, v);
    assert(SUCCEEDED(hr));

    ++fence->value;

    //Gets the current value of the fence
    if (ID3D12Fence_GetCompletedValue(fence->fence) < v)
    {
        //
        hr = ID3D12Fence_SetEventOnCompletion(fence->fence, v, fence->event);
        assert(SUCCEEDED(hr));
        demo_wait_event(fence->event, INFINITE);
    }

    cxr->frame_idx = demo_swapchain_get_current_back_buffer_index(cxr->swapchain);
}

static void cxr_render_frame(struct demo_window *window, void *user_data)
{
    struct cx_rectangle *cxr = user_data;

    cxr_populate_command_list(cxr);
    ID3D12CommandQueue_ExecuteCommandLists(cxr->command_queue, 1, (ID3D12CommandList **)&cxr->command_list);
    demo_swapchain_present(cxr->swapchain);
    cxr_wait_for_previous_frame(cxr);
}

static void cxr_destroy_pipeline(struct cx_rectangle *cxr)
{
    unsigned int i;

    ID3D12CommandAllocator_Release(cxr->command_allocator);
    for (i = 0; i < ARRAY_SIZE(cxr->render_targets); ++i)
    {
        ID3D12Resource_Release(cxr->render_targets[i]);
    }
    ID3D12DescriptorHeap_Release(cxr->rtv_heap);
    demo_swapchain_destroy(cxr->swapchain);
    ID3D12CommandQueue_Release(cxr->command_queue);
    ID3D12Device_Release(cxr->device);
}

static void cxr_load_pipeline(struct cx_rectangle *cxr)
{
    struct demo_swapchain_desc swapchain_desc;
    //Describes the descriptor heap.
    D3D12_DESCRIPTOR_HEAP_DESC rtv_heap_desc;
    D3D12_CPU_DESCRIPTOR_HANDLE rtv_handle;
    //Describes a command queue.
    D3D12_COMMAND_QUEUE_DESC queue_desc;
    unsigned int i;
    HRESULT hr;

    //Creates a device that represents the display adapter.
    hr = D3D12CreateDevice(NULL, D3D_FEATURE_LEVEL_11_0, &IID_ID3D12Device, (void **)&cxr->device);
    assert(SUCCEEDED(hr));

    memset(&queue_desc, 0, sizeof(queue_desc));
    queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
    queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
    //Creates a command queue.
    hr = ID3D12Device_CreateCommandQueue(cxr->device, &queue_desc,
            &IID_ID3D12CommandQueue, (void **)&cxr->command_queue);
    assert(SUCCEEDED(hr));

    swapchain_desc.buffer_count = ARRAY_SIZE(cxr->render_targets);
    swapchain_desc.format = DXGI_FORMAT_B8G8R8A8_UNORM;
    swapchain_desc.width = cxr->width;
    swapchain_desc.height = cxr->height;
    //create demo swapchain
    cxr->swapchain = demo_swapchain_create(cxr->command_queue, cxr->window, &swapchain_desc);
    assert(cxr->swapchain);
    //Gets the index of the swap chain's current back buffer.ex
    cxr->frame_idx = demo_swapchain_get_current_back_buffer_index(cxr->swapchain);

    memset(&rtv_heap_desc, 0, sizeof(rtv_heap_desc));
    rtv_heap_desc.NumDescriptors = ARRAY_SIZE(cxr->render_targets);
    rtv_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
    rtv_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    //Creates a descriptor heap object.
    hr = ID3D12Device_CreateDescriptorHeap(cxr->device, &rtv_heap_desc,
            &IID_ID3D12DescriptorHeap, (void **)&cxr->rtv_heap);
    assert(SUCCEEDED(hr));

    //Gets the size of the handle increment for the given type of descriptor heap. 
    cxr->rtv_descriptor_size = ID3D12Device_GetDescriptorHandleIncrementSize(cxr->device,
            D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    //Gets the CPU descriptor handle that represents the start of the heap.
    rtv_handle = ID3D12DescriptorHeap_GetCPUDescriptorHandleForHeapStart(cxr->rtv_heap);
    for (i = 0; i < ARRAY_SIZE(cxr->render_targets); ++i)
    {
        cxr->render_targets[i] = demo_swapchain_get_back_buffer(cxr->swapchain, i);
        //Creates a render-target view for accessing resource data.
        ID3D12Device_CreateRenderTargetView(cxr->device, cxr->render_targets[i], NULL, rtv_handle);
        rtv_handle.ptr += cxr->rtv_descriptor_size;
    }

    //Creates a command allocator object.
    hr = ID3D12Device_CreateCommandAllocator(cxr->device, D3D12_COMMAND_LIST_TYPE_DIRECT,
            &IID_ID3D12CommandAllocator, (void **)&cxr->command_allocator);
    assert(SUCCEEDED(hr));
}

static void cxr_fence_destroy(struct cxr_fence *cxr_fence)
{
    ID3D12Fence_Release(cxr_fence->fence);
    demo_destroy_event(cxr_fence->event);
}

static void cxr_destroy_assets(struct cx_rectangle *cxr)
{
    cxr_fence_destroy(&cxr->fence);
    ID3D12Resource_Release(cxr->ib);
    ID3D12Resource_Release(cxr->vb);
    ID3D12GraphicsCommandList_Release(cxr->command_list);
    ID3D12PipelineState_Release(cxr->pipeline_state);
    ID3D12RootSignature_Release(cxr->root_signature);
}

static void cxr_fence_create(struct cxr_fence *fence, ID3D12Device *device)
{
    HRESULT hr;
    //Creates a fence object.
    hr = ID3D12Device_CreateFence(device, 0, D3D12_FENCE_FLAG_NONE,
            &IID_ID3D12Fence, (void **)&fence->fence);
    assert(SUCCEEDED(hr));
    fence->value = 1;
    fence->event = demo_create_event();
    assert(fence->event);
}

static void cxr_load_assets(struct cx_rectangle *cxr)
{
    //Describes elements for the input-assembler stage of the graphics pipeline.
    static const D3D12_INPUT_ELEMENT_DESC il_desc[] =
    {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
        {"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
        {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0}
    };

    const struct
    {
        struct demo_vec3 position;
        struct demo_vec4 colour;
        struct demo_vec2 texcoord;
    }
    //define vertices
    vertices[] =
    {
        {{ -0.5f, -0.5f,  0.0f}, {1.0f, 0.0f, 0.0f, 1.0f},{0.0f,0.0f}},//0
        {{ -0.5f,  0.5f,  0.0f}, {0.0f, 1.0f, 0.0f, 1.0f},{0.0f,0.0f}},//1
        {{  0.5f,  0.5f,  0.0f}, {0.0f, 1.0f, 1.0f, 1.0f},{0.0f,0.0f}},//2
        {{  0.5f, -0.5f,  0.0f}, {0.0f, 0.0f, 1.0f, 1.0f},{0.0f,0.0f}},//3
    };
    //define the sequence of vertices
    uint32_t indices[] = {
        0, 1, 2,
        0, 2, 3,
    };

    //Describes the layout of a root signature 
    D3D12_ROOT_SIGNATURE_DESC root_signature_desc;
    //Describes a graphics pipeline state object.
    D3D12_GRAPHICS_PIPELINE_STATE_DESC pso_desc;
    //Describes a resource, such as a texture. 
    D3D12_RESOURCE_DESC resource_desc;
    //Describes heap properties.
    D3D12_HEAP_PROPERTIES heap_desc;
    //Describes a memory range.
    D3D12_RANGE read_range = {0, 0};
    HRESULT hr;
    void *data_ib;
    void *data_vb;

    //the value of root_signature_desc is initialized to 0
    memset(&root_signature_desc, 0, sizeof(root_signature_desc));
    root_signature_desc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
    //Creates a root signature layout.
    hr = demo_create_root_signature(cxr->device, &root_signature_desc, &cxr->root_signature);
    assert(SUCCEEDED(hr));

    memset(&pso_desc, 0, sizeof(pso_desc));
    //describe the data types of the input-assembler stage.
    pso_desc.InputLayout.pInputElementDescs = il_desc;
    pso_desc.InputLayout.NumElements = ARRAY_SIZE(il_desc);
    pso_desc.pRootSignature = cxr->root_signature;
    //VS---A D3D12_SHADER_BYTECODE structure that describes the vertex shader.
    pso_desc.VS.pShaderBytecode = g_VSMain;//in circle_vs.h
    pso_desc.VS.BytecodeLength = sizeof(g_VSMain);
    //PS---A D3D12_SHADER_BYTECODE structure that describes the pixel shader.
    pso_desc.PS.pShaderBytecode = g_PSMain;//in circle_ps.h
    pso_desc.PS.BytecodeLength = sizeof(g_PSMain);
    demo_rasterizer_desc_init_default(&pso_desc.RasterizerState);
    demo_blend_desc_init_default(&pso_desc.BlendState);
    pso_desc.DepthStencilState.DepthEnable = FALSE;
    pso_desc.DepthStencilState.StencilEnable = FALSE;
    pso_desc.SampleMask = UINT_MAX;
    pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    pso_desc.NumRenderTargets = 1;
    pso_desc.RTVFormats[0] = DXGI_FORMAT_B8G8R8A8_UNORM;
    pso_desc.SampleDesc.Count = 1;

    //Creates a graphics pipeline state object.
    hr = ID3D12Device_CreateGraphicsPipelineState(cxr->device, &pso_desc,
            &IID_ID3D12PipelineState, (void **)&cxr->pipeline_state);
    assert(SUCCEEDED(hr));

    //Creates a command list.
    hr = ID3D12Device_CreateCommandList(cxr->device, 0, D3D12_COMMAND_LIST_TYPE_DIRECT, cxr->command_allocator,
            cxr->pipeline_state, &IID_ID3D12GraphicsCommandList, (void **)&cxr->command_list);
    assert(SUCCEEDED(hr));

    //Indicates that recording to the command list has finished.
    hr = ID3D12GraphicsCommandList_Close(cxr->command_list);
    assert(SUCCEEDED(hr));

    heap_desc.Type = D3D12_HEAP_TYPE_UPLOAD;
    heap_desc.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    heap_desc.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    heap_desc.CreationNodeMask = 1;
    heap_desc.VisibleNodeMask = 1;

    resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
    resource_desc.Alignment = 0;
    resource_desc.Width = sizeof(vertices);
    resource_desc.Height = 1;
    // DepthOrArraySize: Specifies the depth of the resource, if it is 3D, or the array size if it is an array of 1D or 2D resources.
    resource_desc.DepthOrArraySize = 1; 
    resource_desc.MipLevels = 1;
    resource_desc.Format = DXGI_FORMAT_UNKNOWN;
    resource_desc.SampleDesc.Count = 1;
    resource_desc.SampleDesc.Quality = 0;
    resource_desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
    resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;

    //Creates both a resource and an implicit heap, such that the heap is big enough to contain the entire resource, and the resource is mapped to the heap.
    hr = ID3D12Device_CreateCommittedResource(cxr->device, &heap_desc, D3D12_HEAP_FLAG_NONE, &resource_desc,
            D3D12_RESOURCE_STATE_GENERIC_READ, NULL, &IID_ID3D12Resource, (void **)&cxr->vb);
    assert(SUCCEEDED(hr));

    resource_desc.Width = sizeof(indices);
    hr = ID3D12Device_CreateCommittedResource(cxr->device, &heap_desc, D3D12_HEAP_FLAG_NONE, &resource_desc,
            D3D12_RESOURCE_STATE_GENERIC_READ, NULL, &IID_ID3D12Resource, (void **)&cxr->ib);
    assert(SUCCEEDED(hr));

    //Gets a CPU pointer to the specified subresource in the resource, but may not disclose the pointer value to applications. 
    //data: A pointer to a memory block that receives a pointer to the resource data.
    hr = ID3D12Resource_Map(cxr->vb, 0, &read_range, &data_vb);
    assert(SUCCEEDED(hr));
    memcpy(data_vb, vertices, sizeof(vertices));

    hr = ID3D12Resource_Map(cxr->ib, 0, &read_range, &data_ib);
    assert(SUCCEEDED(hr));
    memcpy(data_ib, indices, sizeof(indices));

    //Invalidates the CPU pointer to the specified subresource in the resource.
    ID3D12Resource_Unmap(cxr->vb, 0, NULL);
    ID3D12Resource_Unmap(cxr->ib, 0, NULL);

    //returns the GPU virtual address of a buffer resource.
    cxr->vbv.BufferLocation = ID3D12Resource_GetGPUVirtualAddress(cxr->vb);
    cxr->vbv.StrideInBytes = sizeof(*vertices);
    cxr->vbv.SizeInBytes = sizeof(*vertices) * 4;

    cxr->ibv.BufferLocation = ID3D12Resource_GetGPUVirtualAddress(cxr->ib);
    cxr->ibv.SizeInBytes = sizeof(indices);
    cxr->ibv.Format = DXGI_FORMAT_R32_UINT;

    cxr_fence_create(&cxr->fence, cxr->device);
    cxr_wait_for_previous_frame(cxr);
}

static void cxr_key_press(struct demo_window *window, demo_key key, void *user_data)
{
    if (key == DEMO_KEY_ESCAPE)
        demo_window_destroy(window);
}

static int cxr_main(void)
{
    unsigned int width = 640, height = 480;
    struct cx_rectangle cxr;

    memset(&cxr, 0, sizeof(cxr));

    if (!demo_init(&cxr.demo, NULL))
        return EXIT_FAILURE;

    cxr.window = demo_window_create(&cxr.demo, "Vkd3d Circle", width, height, &cxr);
    demo_window_set_expose_func(cxr.window, cxr_render_frame);
    demo_window_set_key_press_func(cxr.window, cxr_key_press);

    cxr.width = width;
    cxr.height = height;
    cxr.aspect_ratio = (float)width / (float)height;

    cxr.vp.Width = (float)width;
    cxr.vp.Height = (float)height;
    cxr.vp.MaxDepth = 1.0f;

    cxr.scissor_rect.right = width;
    cxr.scissor_rect.bottom = height;

    cxr_load_pipeline(&cxr);
    cxr_load_assets(&cxr);

    demo_process_events(&cxr.demo);

    cxr_wait_for_previous_frame(&cxr);
    cxr_destroy_assets(&cxr);
    cxr_destroy_pipeline(&cxr);
    demo_cleanup(&cxr.demo);

    return EXIT_SUCCESS;
}

#ifdef _WIN32
int wmain(void)
#else
int main(void)
#endif
{
    return cxr_main();
}
